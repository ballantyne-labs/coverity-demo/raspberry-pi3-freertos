FreeRTOS V10.2.1 ported to Raspberry Pi 3 (64bit) on Qemu
================================================================

Emulation
---------

* Install the AArch64 Toolchain
```sh
$ sudo apt-get install gcc-aarch64-linux-gnu
```

* Obtain the latest Qemu
```sh
$ wget https://download.qemu.org/qemu-4.1.0.tar.xz
$ tar -xJf qemu-4.1.0.tar.xz
$ cd qemu-4.1.0.tar.xz
$ ./configure --target-list=aarch64-softmmu --enable-modules --enable-tcg-interpreter
$ make -j8
$ sudo make install
```

* Run the kernel on Qemu
```sh
$ make run
qemu-system-aarch64 -M raspi3 -serial stdio -kernel build/kernel8.elf
VNC server running on 127.0.0.1:5900
[FreeRTOS] Hello World!
System Ticks: 1
System Ticks: 501
System Ticks: 1001
```

For more information, see Raspberry Pi firmware wiki and documentation on github.

https://github.com/raspberrypi



