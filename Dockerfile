ARG VERSION
FROM registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$VERSION
RUN apt-get update -q && \
    apt-get -y install gcc-aarch64-linux-gnu libpixman-1-dev pkg-config libglib2.0-dev bison flex && \
    wget -nv https://download.qemu.org/qemu-4.1.0.tar.xz && \
    tar -xJf qemu-4.1.0.tar.xz && \
    rm qemu-4.1.0.tar.xz && \
    mv qemu-4.1.0 /opt/ &&\
    cd /opt/qemu-4.1.0 &&\
    ./configure --target-list=aarch64-softmmu --enable-modules --enable-tcg-interpreter &&\
    make -j && \
    make install
